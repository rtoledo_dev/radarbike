class CreateRacePhotos < ActiveRecord::Migration
  def change
    create_table :race_photos do |t|
      t.attachment :photo
      t.references :race, index: true, foreign_key: true
      t.boolean :master

      t.timestamps null: false
    end
  end
end
