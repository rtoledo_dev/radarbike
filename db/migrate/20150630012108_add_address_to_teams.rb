class AddAddressToTeams < ActiveRecord::Migration
  def change
    change_table :teams do |t|
      t.string :zipcode
      t.string :city
      t.string :state
      t.string :address
    end
  end
end
