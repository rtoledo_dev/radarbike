class CreatePostPhotos < ActiveRecord::Migration
  def change
    create_table :post_photos do |t|
      t.attachment :photo
      t.references :post, index: true, foreign_key: true
      t.boolean :master

      t.timestamps null: false
    end
  end
end
