class ChangeRobberyBikeParts < ActiveRecord::Migration
  def up
    create_table :robbery_bike_parts, force: true do |t|
      t.references :robbery, index: true, foreign_key: true
      t.string :part_name
      t.string :part_value

      t.timestamps null: false
    end
  end

  def down
    create_table :robbery_bike_parts, force: true do |t|
      t.references :robbery, index: true, foreign_key: true
      t.references :bike_part, index: true, foreign_key: true
      t.string :part_value

      t.timestamps null: false
    end
  end
end
