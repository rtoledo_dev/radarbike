class CreateRobberyPhotos < ActiveRecord::Migration
  def change
    create_table :robbery_photos do |t|
      t.attachment :photo
      t.references :robbery, index: true, foreign_key: true
      t.boolean :master

      t.timestamps null: false
    end
  end
end
