class AddShowTitleInIndexToRaces < ActiveRecord::Migration
  def change
    add_column :races, :show_title_in_index, :boolean
  end
end
