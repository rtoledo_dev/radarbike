class CreateRobberies < ActiveRecord::Migration
  def change
    create_table :robberies do |t|
      t.string :description
      t.string :robbery_type
      t.references :robbery, index: true, foreign_key: true
      t.boolean :active

      t.timestamps null: false
    end
  end
end
