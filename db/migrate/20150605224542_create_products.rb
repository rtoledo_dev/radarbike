class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :product_type
      t.text :description
      t.float :price
      t.date :expire_at
      t.boolean :active

      t.timestamps null: false
    end
  end
end
