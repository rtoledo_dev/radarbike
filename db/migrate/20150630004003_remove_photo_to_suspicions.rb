class RemovePhotoToSuspicions < ActiveRecord::Migration
  def change
    remove_column :suspicions, :photo_file_name
    remove_column :suspicions, :photo_content_type
    remove_column :suspicions, :photo_file_size
    remove_column :suspicions, :photo_updated_at
  end
end
