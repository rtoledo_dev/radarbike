class CreateSuspicionBikeParts < ActiveRecord::Migration
  def change
    create_table :suspicion_bike_parts do |t|
      t.references :suspicion, index: true, foreign_key: true
      t.string :part_name
      t.string :part_value

      t.timestamps null: false
    end
  end
end
