class CreateRobberyBikeParts < ActiveRecord::Migration
  def change
    create_table :robbery_bike_parts do |t|
      t.references :robbery, index: true, foreign_key: true
      t.references :bike_part, index: true, foreign_key: true
      t.string :part_value

      t.timestamps null: false
    end
  end
end
