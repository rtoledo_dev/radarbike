class ChangeRobberies < ActiveRecord::Migration
  def up
    execute "SET FOREIGN_KEY_CHECKS=0"
    create_table :robberies, force: true do |t|
      t.string :description
      t.references :user, index: true, foreign_key: true
      t.boolean :is_public, default: true

      t.string :zipcode
      t.string :city
      t.string :state
      t.string :address
      t.text :details

      t.timestamps null: false
    end
  end

  def down
    execute "SET FOREIGN_KEY_CHECKS=0"
    create_table :robberies, force: true do |t|
      t.string :description
      t.string :robbery_type
      t.references :user, index: true, foreign_key: true
      t.references :robbery, index: true, foreign_key: true
      t.boolean :active

      t.timestamps null: false
    end
  end
end
