class AddContactToRobberies < ActiveRecord::Migration
  def change
    change_table :robberies do |t|
      t.string :contact_name
      t.string :contact_email
      t.string :contact_phone
      t.string :contact_facebook
    end
  end
end
