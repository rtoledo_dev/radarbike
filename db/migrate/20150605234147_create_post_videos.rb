class CreatePostVideos < ActiveRecord::Migration
  def change
    create_table :post_videos do |t|
      t.attachment :video
      t.references :post, index: true, foreign_key: true
      t.boolean :master

      t.timestamps null: false
    end
  end
end
