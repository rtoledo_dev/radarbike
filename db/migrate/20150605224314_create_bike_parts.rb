class CreateBikeParts < ActiveRecord::Migration
  def change
    create_table :bike_parts do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
