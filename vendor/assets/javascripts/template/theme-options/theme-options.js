// THEME OPTIONS.JS
//--------------------------------------------------------------------------------------------------------------------------------
//This is JS file that contains skin, layout Style and bg used in this template*/
// -------------------------------------------------------------------------------------------------------------------------------
// Template Name: Sports Cup- Responsive HTML5  soccer and sports Template.
// Author: Iwthemes.
// Name File: theme-options.js
// Version 1.0 - Created on 20 May 2014
// Website: http://www.iwthemes.com
// Email: support@iwthemes.com
// Copyright: (C) 2014
// -------------------------------------------------------------------------------------------------------------------------------

  $(document).ready(function($) {

  	/* Selec your skin and layout Style */

	function interface(){

    // Skin value
    var skin = "green"; // green (default), red ,yellow,purple,blue, orange, purple, pink, cocoa, custom

    // Theme Panel - disable panel options
    var themepanel = "0"; // 1 (default - enable), 0 ( disable)

    $(".skin").attr("href", "/assets/template/skins/"+ skin + "/" + skin + ".css");
    $("#theme-options").css('opacity' , themepanel);
    return false;
  }

 	interface();

	//=================================== Skins Changer ====================================//

    // Color changer
    $(".red").click(function(){
	   	$(".skin").attr("href", "/assets/template/skins/red/red.css");
	    return false;
	});
	$(".blue").click(function(){
	    $(".skin").attr("href", "/assets/template/skins/blue/blue.css");
	    return false;
	});
	$(".yellow").click(function(){
	    $(".skin").attr("href", "/assets/template/skins/yellow/yellow.css");
	    return false;
	});
	$(".green").click(function(){
	    $(".skin").attr("href", "/assets/template/skins/green/green.css");
	    return false;
	});
	$(".orange").click(function(){
    	$(".skin").attr("href", "/assets/template/skins/orange/orange.css");
    	return false;
	});
	$(".purple").click(function(){
	    $(".skin").attr("href", "/assets/template/skins/purple/purple.css");
	    return false;
	});
	$(".pink").click(function(){
	    $(".skin").attr("href", "/assets/template/skins/pink/pink.css");
	    return false;
	});
	$(".cocoa").click(function(){
        $(".skin").attr("href", "/assets/template/skins/cocoa/cocoa.css");
        return false;
   	});

	//=================================== Background Options ====================================//

	$('#theme-options ul.backgrounds li').click(function(){
	var 	$bgSrc = $(this).css('background-image');
		if ($(this).attr('class') == 'bgnone')
			$bgSrc = "none";

		$('body').css('background-image',$bgSrc);
		$.cookie('background', $bgSrc);
		$.cookie('backgroundclass', $(this).attr('class').replace(' active',''));
		$(this).addClass('active').siblings().removeClass('active');
	});

	//=================================== Panel Options ====================================//

	$('.openclose').click(function(){
		if ($('#theme-options').css('left') == "-80px")
		{
			$left = "0px";
			$.cookie('displayoptions', "0");
		} else {
			$left = "-80px";
			$.cookie('displayoptions', "1");
		}
		$('#theme-options').animate({
			left: $left
		},{
			duration: 500
		});

	});

	$(function(){
		$('#theme-options').fadeIn();
		$bgSrc = $.cookie('background');
		$('body').css('background-image',$bgSrc);

		if ($.cookie('displayoptions') == "1")
		{
			$('#theme-options').css('left','-80px');
		} else if ($.cookie('displayoptions') == "0") {
			$('#theme-options').css('left','0');
		} else {
			$('#theme-options').delay(800).animate({
				left: "-80px"
			},{
				duration: 500
			});
			$.cookie('displayoptions', "1");
		}
		$('#theme-options ul.backgrounds').find('li.' + $.cookie('backgroundclass')).addClass('active');

	});

});
