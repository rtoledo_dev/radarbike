Rails.application.routes.draw do
  require 'sidekiq/web'
  authenticate :admin do
    mount Sidekiq::Web => '/sidekiq'
  end
  devise_for :admins
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  get 'home' => "home#index", as: :home
  get 'sobre-a-empresa' => "pages#about", as: :about
  get 'divulgacoes-e-parcerias' => "pages#advertising", as: :advertising
  get 'fale-conosco' => "contact#index", as: :contact
  post 'enviar-contato' => "contact#send_contact", as: :send_contact

  resources :teams, except: :show do
    resources :pictures do
      member do
        get :make_master
      end
    end
  end
  resources :races, except: :show do
    member do
      get :send_notifications
    end
    resources :pictures do
      member do
        get :make_master
      end
    end
  end
  resources :products, except: :show do
    resources :pictures do
      member do
        get :make_master
      end
    end
  end
  resources :posts, except: :show do
    resources :pictures do
      member do
        get :make_master
      end
    end
  end
  resources :post_categories
  resources :public_products, only: :show, path: :produtos
  resources :public_post_categories, only: :show, path: 'posts-por-categoria'
  resources :public_races, only: [:index, :show], path: 'provas', path_names: {calendar: 'calendario', last: 'ultimos-resultados', race_organization: 'organize-sua-prova', send_contact: 'enviar-mensagem'} do
    collection do
      get :calendar
      get :last
      get :race_organization
      post :send_contact
    end
  end
  resources :public_teams, only: [:index, :show], path: 'equipes', path_names: {special: 'oficiais'} do
    collection do
      get :special
    end
  end
  get 'posts/:id' => 'public_posts#show', as: :public_post
  resources :public_robberies, path: 'furtos', path_names: { new: 'registrar', edit: 'atualizar', destroy: 'apagar' }
  resources :public_suspicions, path: 'suspeitas', path_names: { new: 'registrar', edit: 'atualizar', destroy: 'apagar' }

  get 'minha-conta' => 'my_account#index', as: :my_account
  put 'atualizar-minha-conta' => 'my_account#update', as: :update_my_account

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
