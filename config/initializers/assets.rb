# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += ['template/bootstrap/bootstrap.css', 'template/bootstrap/bootstrap-theme.css', 'template/icons/font-awesome.css', 'template/transitions.css', 'template/slide-revolution/settings.css', 'template/slide-revolution/extralayers.css', 'template/fancybox/jquery.fancybox.css', 'template/carousel/carousel.css', 'template/login/login.css', 'template/flickr/flickr.css', 'template/nav/megafish.css', 'template/animations/animate.css', 'template/slide/responsiveslides.css', 'template/skins/theme-options.css', 'template/scrollbar/jquery.mCustomScrollbar.css']
Rails.application.config.assets.precompile += ['fullcalendar-2.4.0/moment.js', 'fullcalendar-2.4.0/lang/pt-br.js']

