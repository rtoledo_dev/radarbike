class PostCategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :post_post_categories
  has_many :posts, through: :post_post_categories

  validates :name, presence: true, uniqueness: true
end
