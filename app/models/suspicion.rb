class Suspicion < ActiveRecord::Base
  attr_accessor :suspicion_picture

  has_many :suspicion_bike_parts
  has_many :pictures, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :suspicion_bike_parts
  validates :description, presence: true

  def picture
    @picture ||= begin
      suspicion_picture = pictures.where(master: true).first
      suspicion_picture.try(:photo)
    end
  end

  after_save do
    if self.suspicion_picture
      Picture.transaction do
        self.pictures.update_all(master: false)
        self.pictures.build(master: true, photo: self.suspicion_picture).save!
      end
    end
  end
end
