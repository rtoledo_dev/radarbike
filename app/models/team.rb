class Team < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  alias_attribute :title, :name

  attr_accessor :team_picture

  has_many :pictures, as: :imageable, dependent: :destroy
  has_many :team_members

  accepts_nested_attributes_for :team_members, reject_if: proc { |attributes| attributes['member'].blank? }, allow_destroy: true

  validates :name, :description, presence: true
  validates :team_picture, presence: true, on: :create

  def picture
    @picture ||= begin
      team_picture = pictures.where(master: true).first
      team_picture.try(:photo)
    end
  end

  after_save do
    if self.team_picture
      Picture.transaction do
        self.pictures.update_all(master: false)
        self.pictures.build(master: true, photo: self.team_picture).save!
      end
    end
  end
end
