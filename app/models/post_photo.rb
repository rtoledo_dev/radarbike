class PostPhoto < ActiveRecord::Base
  belongs_to :post
  has_attached_file :photo, styles: { medium: "600x400#", thumb: "100x100>", gallery_list: "176x118#" }
  validates_attachment_content_type :photo, content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
