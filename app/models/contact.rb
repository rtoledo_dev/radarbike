class Contact
  include ActiveModel::Model

  attr_accessor :name, :visitor_email, :message, :subject

  validates_presence_of :name, :message, :subject, :visitor_email
  validates :visitor_email, email: true
  def deliver
    return false unless self.valid?
    ContactMailer.new_contact(self.name, self.visitor_email, self.subject, self.message).deliver
  end
end
