class RobberyBikePart < ActiveRecord::Base
  belongs_to :robbery

  validates :part_name, :part_value, presence: true
end
