class Robbery < ActiveRecord::Base
  attr_accessor :robbery_picture

  belongs_to :robbery
  belongs_to :user
  has_many :robbery_bike_parts
  has_many :pictures, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :robbery_bike_parts

  validates :description, presence: true
  validates :robbery_picture, presence: true, on: :create
  validates :contact_name, :contact_email, presence: true, unless: :user_signed_in?


  def user_signed_in?
    not self.user_id.blank?
  end

  def picture
    @picture ||= begin
      robbery_picture = pictures.where(master: true).first
      robbery_picture.try(:photo)
    end
  end

  after_save do
    if self.robbery_picture
      Picture.transaction do
        self.pictures.update_all(master: false)
        self.pictures.build(master: true, photo: self.robbery_picture).save!
      end
    end
  end
end
