class Picture < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true
  has_attached_file :photo, styles: { big: "1024", medium: "600x400#", thumb: "100x100>", gallery_list: "176x118#", team_list: '1024x768#' }, default_style: :big
  validates_attachment_content_type :photo, content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
