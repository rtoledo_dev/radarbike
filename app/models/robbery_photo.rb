class RobberyPhoto < ActiveRecord::Base
  belongs_to :robbery
  has_attached_file :photo, styles: { big: "1024x1024>", medium: "300x300>", thumb: "100x100>", gallery_list: "176x118#" }, default_style: :big
  validates_attachment_content_type :photo, content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
