json.array!(@posts) do |post|
  json.extract! post, :id, :user_id, :title, :resume, :content, :published_at, :active
  json.url post_url(post, format: :json)
end
