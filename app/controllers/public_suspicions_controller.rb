class PublicSuspicionsController < ApplicationController
  before_action :set_suspicion, only: [:show]

  def new
    @suspicion = Suspicion.new
  end

  def create
    @suspicion = Suspicion.new(suspicion_params)

    if @suspicion.save
      redirect_to @suspicion, notice: 'Suspicion was successfully created.'
    else
      flash[:error] = t('.error_on_create')
      render :new
    end
  end

  # GET /suspicions/1
  # GET /suspicions/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suspicion
      @suspicion = current_user.robberies.find(params[:id])
    end

    def suspicion_params
      params.require(:suspicion).permit(:description, :zipcode, :state, :city,
        :address, :photo,
        suspicion_bike_parts_attributes: [:id, :part_name, :part_value, :_destroy])
    end
end
