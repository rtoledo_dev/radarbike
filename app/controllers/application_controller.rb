class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :prepare_top_menu
  before_action :configure_permitted_parameters, if: :devise_controller?
  protected

  def configure_permitted_parameters
    [:name, :info, :send_races].each do |t|
      devise_parameter_sanitizer.for(:sign_up) << t
    end
  end

  def prepare_top_menu
    @menu_post_categories = PostCategory.order(:name)
  end
end
