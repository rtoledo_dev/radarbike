class MyAccountController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def update
    if current_user.update_without_password(user_params)
      redirect_to :back, notice: 'Suas informações foram salvas com sucesso'
    else
      flash[:notice] = 'Erro ao salvar sua informações verifique os campos abaixo'
      render :index
    end
  end

  private
    def user_params
      params[:user].permit(:name, :info, :photo, :zipcode, :address, :state, :city, :specialty,
        :facebook_url, :send_races)
    end
end
