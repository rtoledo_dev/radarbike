class PublicPostCategoriesController < ApplicationController
  before_action :set_post_category, only: [:show]

  # GET /post_categorys/1
  # GET /post_categorys/1.json
  def show
    @posts = @post_category.posts.actives.ordered.page(params[:page])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_category
      @post_category = PostCategory.friendly.find(params[:id])
    end
end
