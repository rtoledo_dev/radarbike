class PublicRacesController < ApplicationController
  before_action :set_race, only: [:show]

  def index
    @races = Race.where("occur_at > ?", Date.today).order(occur_at: :desc).page(params[:page])
  end

  def last
    @races = Race.where("occur_at <= ?", Date.today).order(occur_at: :desc).page(params[:page])
  end

  # GET /races/1
  # GET /races/1.json
  def show
  end

  def race_organization
    @race_organization_contact = RaceOrganizationContact.new
  end

  def send_contact
    @race_organization_contact = RaceOrganizationContact.new(race_organization_contact_params)
    @race_organization_contact.subject = 'Novo interesse em organização de prova'
    if @race_organization_contact.deliver
      redirect_to race_organization_public_races_path, success: 'Mensagem enviada com sucesso. Em breve entraremos em contato.'
    else
      flash[:error] = 'Falha ao enviar a mensagem. Todos os campos são obrigatórios'
      render :race_organization
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_race
      @race = Race.friendly.find(params[:id])
    end

    def race_organization_contact_params
      params[:race_organization_contact].permit(:name, :visitor_email, :message, :occur_at, :motive)
    end
end
