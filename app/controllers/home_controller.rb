class HomeController < ApplicationController
  def index
    @products = Product.actives.order(created_at: :desc).limit(10)
    @race_special = Race.order("occur_at desc").first
    @races = Race.where("id <> ?", @race_special.try(:id)).order(occur_at: :desc).limit(3)
    @team = Team.where(special: true).order("rand()").first
    @post = Post.actives.ordered.first
  end

  def invite_user
    User.invite!(:email => params[:email])
  end
end
