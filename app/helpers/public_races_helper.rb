module PublicRacesHelper
  def race_organization_message_placeholder
    "Escreva mais informações sobre a prova como:
- Motivo da prova (aniversário da cidade, data comemorativa):
- Data:
- Horário:
- Estilo preferido (speed, montanhas, etc...):

Em breve entraremos em contato"
  end

  def races_events
    Race.where("occur_at >= ?",Date.today.beginning_of_year).map do |race|
      {title: race.title, url: public_race_path(race), start: race.occur_at.strftime("%Y-%m-%d")}
    end.to_json.html_safe
  end
end
