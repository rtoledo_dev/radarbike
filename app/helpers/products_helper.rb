module ProductsHelper
  def product_price(product)
    product.price ? number_to_currency(product.price) : 'A consultar'
  end
end
