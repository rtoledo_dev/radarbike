class ContactMailer < ActionMailer::Base
  layout 'email'
  default from: "contatoradarbike@gmail.com"
  helper :application

  def new_contact(name,visitor_email,subject,message)
    @name           = name
    @subject        = subject
    @visitor_email  = visitor_email
    @message        = message

    mail to: "contatoradarbike@gmail.com", bcc: ['rodrigo@rtoledo.inf.br'], reply_to: @visitor_email
  end

  def new_contact_of_race_organization(name,visitor_email,subject,message)
    @name           = name
    @subject        = subject
    @visitor_email  = visitor_email
    @message        = message

    mail to: ["contatoradarbike@gmail.com"],
      bcc: ['rodrigo@rtoledo.inf.br'],
      reply_to: @visitor_email
  end
end
