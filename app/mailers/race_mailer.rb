class RaceMailer < ActionMailer::Base
  layout 'email'
  default from: "contatoradarbike@gmail.com"
  helper :application

  def new_race(user,race)
    @race = race

    mail to: user.email, subject: 'Radar Bike - Nova Prova'
  end
end
